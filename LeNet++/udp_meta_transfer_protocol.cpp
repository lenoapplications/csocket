#include "pch.h"
#include "udp_meta_transfer_protocol.h"

namespace basicMetaUdpProtocol
{

	const char* META_BEGIN_SIZE = "<SzB>";
	const char* META_END_SIZE = "<SzE>";

	const char* BEGIN_MESSAGE = "<1B2>";
	const char* END_MESSAGE = "<3E4>";

	int beginCharSize = 5;
	int endCharSize = 5;

	bool extractMetaDataSize(char* bytes, int bytesRead,int* size)
	{
		int i = 1;
		int lastIndex = bytesRead - 1;

		bool completed =  *(bytes + lastIndex--) == '>' && *(bytes + lastIndex--) == 'E' && *(bytes + lastIndex--) == 'z' && *(bytes + lastIndex--) == 'S' && *(bytes + lastIndex--) == '<';

		if(completed)
		{
			std::string stringBuffer{ bytes };
			int endIndex = stringBuffer.find(basicMetaUdpProtocol::META_END_SIZE);
	
			if (endIndex != std::string::npos)
			{
				*size = std::stoi(stringBuffer.substr(0 + basicMetaUdpProtocol::beginCharSize, endIndex - basicMetaUdpProtocol::endCharSize));
				return true;
			}
		}
		return false;
	}

	bool extractMessage(char* bytes, int bytesRead, int* beginIndex,int* endIndex)
	{
		int i = 1;
		int lastIndex = bytesRead - 1;

		bool completed = *(bytes + lastIndex--) == '>' && *(bytes + lastIndex--) == '4' && *(bytes + lastIndex--) == 'E' && *(bytes + lastIndex--) == '3' && *(bytes + lastIndex--) == '<';

		if (completed)
		{
			
			*beginIndex = 0 + basicMetaUdpProtocol::beginCharSize;
			*endIndex = (lastIndex + 1) - basicMetaUdpProtocol::endCharSize;
			return true;
		}
		return false;
	}
}

