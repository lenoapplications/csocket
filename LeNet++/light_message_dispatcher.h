#pragma once
#include "message_dispatcher_form.h"


template<typename MESSAGE_PROTOCOL>
class LightMessageDispatcher : public MessageDispatcherForm<MESSAGE_PROTOCOL>
{
public:
	LightMessageDispatcher();
	~LightMessageDispatcher();

	void dispatchRawBytes(ByteTransporter* byteTransporter) override;
	void dispatchMessage(std::wstring message);
	void dispatchMessage(std::string message);
protected:
private:
	
};

template <typename MESSAGE_PROTOCOL>
LightMessageDispatcher<MESSAGE_PROTOCOL>::LightMessageDispatcher()
{
}

template <typename MESSAGE_PROTOCOL>
LightMessageDispatcher<MESSAGE_PROTOCOL>::~LightMessageDispatcher()
{
}

template <typename MESSAGE_PROTOCOL>
void LightMessageDispatcher<MESSAGE_PROTOCOL>::dispatchRawBytes(ByteTransporter* byteTransporter)
{
	
}

template <typename MESSAGE_PROTOCOL>
void LightMessageDispatcher<MESSAGE_PROTOCOL>::dispatchMessage(std::wstring message)
{
}

template <typename MESSAGE_PROTOCOL>
void LightMessageDispatcher<MESSAGE_PROTOCOL>::dispatchMessage(std::string message)
{
}

