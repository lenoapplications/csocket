#include "pch.h"
#include "socketActivationConfiguration.h"

SocketActivationConfiguration::~SocketActivationConfiguration()
{
	
}

bool SocketActivationConfiguration::checkIfFlagIsActive(int flag) const
{
	return (this->flagMemory & flag) == flag;
}

void SocketActivationConfiguration::setFlag(int flags)
{
	std::cout << "adding flag  " << flags << std::endl;
	this->flagMemory = flags;
}
