#include "pch.h"
#include "udp_socket.h"

UdpSocket::UdpSocket()
{
	this->sizeOfClientAddress = sizeof(clientsAddress);
	this->metaBuffer = new char[META_BUFFER_SIZE];
	this->messageBuffer = new char[1];
	ZeroMemory(&clientsAddress, sizeOfClientAddress);
}

UdpSocket::~UdpSocket()
{
	delete[] metaBuffer;
	delete[] messageBuffer;
}

void UdpSocket::activateSocketAsServer(std::function<void(char*,int)> dispatchFunction)
{
	if(initWsok())
	{
		bindSocketNumber(SOCK_DGRAM, IPPROTO_UDP);
		bindAfInetAddress(3000);
		bindAddressAndSocket();
		
		mainServerLoop(dispatchFunction);
	}
}

void UdpSocket::activateSocketAsClient()
{
	
}

void UdpSocket::activateSocketAsConnectedClient()
{
}

void UdpSocket::mainServerLoop(std::function<void(char*,int)>& dispatchFunction)
{
	config.setNewFlagMemory<SocketActivationConfiguration>(KEEP_SOCKET_ALIVE);

	auto t1 = std::chrono::high_resolution_clock::now();
	int count = 1;
	int dataSize = 0;
	while(config.checkMemoryFlag<SocketActivationConfiguration>(KEEP_SOCKET_ALIVE))
	{	
		recvMetaForIncomingMessage(&dataSize);
	
		if(dataSize > 0)
		{
			recvMessage(dataSize);
		}

		if (count == 1)
		{
			t1 = std::chrono::high_resolution_clock::now();
		}
		
		if (count == 20)
		{
			break;
		}
		++count;
	}
	

	auto t2 = std::chrono::high_resolution_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	std::cout << "duration  " << duration << std::endl;
}

void UdpSocket::recvMetaForIncomingMessage(int* size)
{
	int bytesRead = recvfrom(
		mySocket,
		metaBuffer,
		META_BUFFER_SIZE,
		0,
		(sockaddr*)&clientsAddress,
		&sizeOfClientAddress);

	basicMetaUdpProtocol::extractMetaDataSize(metaBuffer, bytesRead,size);
}

void UdpSocket::recvMessage(int size)
{
	if(lastMessageBufferSize < size)
	{
		delete[] messageBuffer;
		
		this->messageBuffer = new char[size + 10];
		lastMessageBufferSize = size;
		MESSAGE_BUFFER_SIZE = size + 10;
	}
	
	int bytesRead = recvfrom(
		mySocket,
		messageBuffer,
		MESSAGE_BUFFER_SIZE,
		0,
		(sockaddr*)&clientsAddress,
		&sizeOfClientAddress);

	int begin = 0;
	int end = 0;

	basicMetaUdpProtocol::extractMessage(messageBuffer, bytesRead, &begin, &end);

	std::cout<<std::string{ messageBuffer }.substr(begin, end)<<std::endl;
}



