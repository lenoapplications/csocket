#pragma once
#include "byte_transporter.h"
#include "queue"

template<typename STRING_ENCODING>
class MessageProtocolForm
{
public:
	MessageProtocolForm() = default;
	~MessageProtocolForm();

	void appendByteToStringBuffer(char* byteTransporter,int bytesRead);

	STRING_ENCODING* getStringBuffer();

	void clear();

	virtual bool checkIfBufferIsComplete() = 0;
	virtual bool getMessage(STRING_ENCODING* stringEncoding) = 0;
	
private:
protected:
	STRING_ENCODING stringBuffer;
};

template <typename STRING_ENCODING>
MessageProtocolForm<STRING_ENCODING>::~MessageProtocolForm()
{
}

template <typename STRING_ENCODING>
void MessageProtocolForm<STRING_ENCODING>::appendByteToStringBuffer(char* bytes,int bytesRead)
{
	stringBuffer.append(bytes,bytesRead);
}

template <typename STRING_ENCODING>
STRING_ENCODING* MessageProtocolForm<STRING_ENCODING>::getStringBuffer()
{
	return &stringBuffer;
}

template <typename STRING_ENCODING>
void MessageProtocolForm<STRING_ENCODING>::clear()
{
	stringBuffer.clear();
}


