#include "pch.h"
#include "byte_transporter.h"
#include <vector>

ByteTransporter::ByteTransporter()
{
	this->buffer = new char[BUFFER_SIZE];
}

ByteTransporter::~ByteTransporter()
{
	delete[] buffer;
	std::cout << "deleted" << std::endl;
}

void ByteTransporter::resetMemory()
{
	delete[] buffer;
	this->buffer = new char[BUFFER_SIZE];
}

char* ByteTransporter::getBuffer()
{
	return this->buffer;
}

void ByteTransporter::setBytesRead(int bytesRead)
{
	this->lastNumberOfBytesRead = bytesRead;
}

int ByteTransporter::getBytesRead()
{
	return this->lastNumberOfBytesRead;
}

