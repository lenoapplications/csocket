#pragma once
#include "socket_config_form.h"


#define RUN_ON_SECOND_THREAD 1
#define KEEP_SOCKET_ALIVE 2

class SocketActivationConfiguration : public SocketConfigForm
{
public:
	SocketActivationConfiguration() = default;
	~SocketActivationConfiguration();

	
	bool checkIfFlagIsActive(int flag) const override;
	void setFlag(int flags) override;
	
private:
	int flagMemory = 0b00000000;
protected:
};

