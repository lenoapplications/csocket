#include "pch.h"
#include "utf8_message_protocol.h"
#include "message_protocol_constructor_utilz.h"

UTF8MessageProtocol::UTF8MessageProtocol()
{
	
}

UTF8MessageProtocol::~UTF8MessageProtocol()
{
	
}

bool UTF8MessageProtocol::checkIfBufferIsComplete()
{
	return basicMessageProtocol::checkIfBasicMessageIsComplete(&stringBuffer);
}

bool UTF8MessageProtocol::getMessage(std::basic_string<char>* stringEncoding)
{
	int beginIndex = 0;
	int endIndex = stringBuffer.find(basicMessageProtocol::END);

	if(endIndex != std::string::npos)
	{
		stringEncoding->append(stringBuffer.substr(beginIndex + basicMessageProtocol::beginCharSize, endIndex - basicMessageProtocol::endCharSize));
		stringBuffer = stringBuffer.substr(endIndex + basicMessageProtocol::endCharSize);
		return true;
	}
	return false;
	
}


//void UTF8MessageProtocol::fillMessageQueue()
//{
//
//	int beginIndex = 0;
//	int endIndex = -1;
//	
//	while (beginIndex != std::string::npos)
//	{
//		endIndex = stringBuffer.find(basicMessageProtocol::END);
//	
//		messagesQueue.push(stringBuffer.substr(beginIndex + basicMessageProtocol::beginCharSize, endIndex - basicMessageProtocol::endCharSize));
//		stringBuffer = stringBuffer.substr(endIndex + basicMessageProtocol::endCharSize);
//		
//		beginIndex = stringBuffer.find(basicMessageProtocol::BEGIN);
//	}
//}
