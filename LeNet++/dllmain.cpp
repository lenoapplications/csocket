// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <iostream>
#include "memorySharingUtilz.h"
#include "server.h"
#include "processUtilz.h"
#include "udp_socket.h"
#include <bitset>
#include "socket_configs.h"
#include "threadManager.h"
#include "threadObject.h"
#include "message_protocol_constructor_utilz.h"

/*
 *
 *BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

 *
 *
 *
 * 
 */

class Abstract
{
public:
    Abstract() = default;
    ~Abstract()
    {
	    
    };
    int number = 20;
    virtual void test() = 0;
};




class Concrete1 : public Abstract
{
public:
    Concrete1()
    {
	    
    };
	~Concrete1()
	{
		
	};

    void test()
    {
        std::cout << "concrete1" << std::endl;
    }
private:
protected:
};


class Concrete2 : public Abstract
{
public:
    Concrete2()
    {

    };
    ~Concrete2()
    {

    };

    void test()
    {
        std::cout << "concrete2" << std::endl;
        number = 345;
    }
private:
protected:
};


class All : public Concrete1,public Concrete2
{
public:
    All() = default;
    ~All(){};

};

class Holder
{
public:
    Holder() = default;
    ~Holder(){};

    All all;

	template<typename A>
    void aa()
	{
        ((A)all).test();
	};

    template<typename A>
    void aaa()
    {
        std::cout << ((A)all).number << std::endl;
    };
	
};



struct Test
{

    int flag = 120;
};

#define M 32;



class FB : public ThreadObject
{
public:
	~FB()
	{
        std::cout << "fb object deleted" << std::endl;
	}

	void run() override
	{
        std::cout << "udem ovdje" << std::endl;
        if(test == 10000)
        {
            const char* test1 = "King kong";
        }
        ++test;
	}
    int test = 1;
};



template<typename A>
void test(A a)
{
    a();
}

class C
{
public:
    C()
    {
        buffer[0] = 'a';
    }
    ~C(){};


	char* getBuffer()
	{
        return buffer;
	}
	
private:
    char buffer[5];
};

int main(int cArgs, char* ppszArgs[])
{

    Server<UdpSocket> server;
    server.setConfiguration<SocketActivationConfiguration>(RUN_ON_SECOND_THREAD);

    server.activate();


    std::shared_ptr<Server<UdpSocket>::ListenerThread> listnerThread = server.getListnerThread();

    while (!listnerThread->isThreadReadyToStop()) {}

    return 0;
}
