#include "pch.h"
#include "message_protocol_constructor_utilz.h"


namespace basicMessageProtocol
{
	const char* BEGIN = "<1B2>";
	const char* END = "<3E4>";

	int beginCharSize = 5;
	int endCharSize = 5;

	void constructBasicMessageProtocol(std::string payload, std::string* packetBuffer)
	{
		std::stringstream stream;

		stream << BEGIN << payload.c_str() << END;

		packetBuffer->clear();

		packetBuffer->append(stream.str());
	}

	bool checkIfBasicMessageIsComplete(std::string* packetBuffer)
	{
		std::string::iterator end = packetBuffer->end();

		return *(--end) == '>' && *(--end) == '4' && *(--end) == 'E' && *(--end) == '3' && *(--end) == '<';
	}

}
