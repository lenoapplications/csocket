#pragma once
#include <string>

namespace basicMetaUdpProtocol
{
	extern const char* META_BEGIN_SIZE;
	extern const char* META_END_SIZE;
	
	extern const char* BEGIN_MESSAGE;
	extern const char* END_MESSAGE;

	extern int beginCharSize ;
	extern int endCharSize ;
	

	bool extractMetaDataSize(char* bytes, int bytesRead,int* size);
	bool extractMessage(char* bytes, int bytesRead,int* beginIndex,int* endIndex);
}

