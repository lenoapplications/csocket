#pragma once
#include "socket_form.h"

class TcpSocket : public SocketForm
{

public:
	TcpSocket();
	~TcpSocket();

	void activateSocketAsServer();
	void activateSocketAsClient();
	void recvFromMySocket();
	
protected:
private:
};

