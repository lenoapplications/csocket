#pragma once
#include "socketActivationConfiguration.h"

class SocketConfigs : public SocketActivationConfiguration
{
public:
	SocketConfigs() = default;
	~SocketConfigs();

	template<typename CONFIG>
	void setNewFlagMemory(int flags);

	template<typename CONFIG>
	bool checkMemoryFlag(int flags);
	
protected:
private:
};

template <typename CONFIG>
void SocketConfigs::setNewFlagMemory(int flags)
{
	((CONFIG*)this)->setFlag(flags);
}

template <typename CONFIG>
bool SocketConfigs::checkMemoryFlag(int flag)
{
	return ((CONFIG*)this)->checkIfFlagIsActive(flag);
}

