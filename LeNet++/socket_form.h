#pragma once
#include <WinSock2.h>
#include <threadObject.h>
#include <iostream>
#include "socket_configs.h"
#include "threadManager.h"
#include "byte_transporter.h"
#include <ctime>
#include "message_dispatcher_form.h"
#include <functional>


class SocketForm
{
public:
	
	SocketForm();
	virtual ~SocketForm();


	virtual void activateSocketAsServer(std::function<void(char*,int bytesRead)> function) = 0;
	virtual void activateSocketAsClient() = 0;
	virtual void activateSocketAsConnectedClient() = 0;

	SocketConfigs config;
private:
protected:
	SOCKET mySocket;
	WSADATA wsData;
	WORD version;
	sockaddr_in address;
	int wsok;
	int sizeOfServerAddress;
	ByteTransporter byteTransporter;

	
	bool initWsok();
	void bindAfInetAddress(u_short port);
	void bindSocketNumber(int protocol, int type);
	bool bindAddressAndSocket();

	
};

inline SocketForm::SocketForm()
{
	this->version = MAKEWORD(2, 2);
}

inline SocketForm::~SocketForm()
{
	
}

inline bool SocketForm::initWsok()
{
	this->wsok = WSAStartup(version, &wsData);

	if(wsok == 0)
	{
		return true;
	}else
	{
		std::cout << "Error" << wsok << std::endl;
		return false;
	}
}

inline void SocketForm::bindAfInetAddress(u_short port)
{
	this->address.sin_addr.S_un.S_addr = ADDR_ANY;
	this->address.sin_family = AF_INET;
	this->address.sin_port = htons(port);
	this->sizeOfServerAddress = sizeof(this->address);
}

inline void SocketForm::bindSocketNumber(int protocol,int type)
{
	this->mySocket = WSASocket(AF_INET, protocol, type, NULL, 0, 0);
}

inline bool SocketForm::bindAddressAndSocket()
{
	if(bind(mySocket,(sockaddr*) &address,sizeof(address)) == SOCKET_ERROR)
	{
		return false;
	}
	return true;
}

