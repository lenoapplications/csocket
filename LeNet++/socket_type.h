#pragma once
#include "socket_form.h"



template<typename SOCKET_FORM,typename MESSAGE_DISPATCHER>
class SocketType
{
public:
	SocketType() = default;
	~SocketType();

	virtual void activate() = 0;

	template<typename CONFIG>
	void setConfiguration(int config);

	template<typename CONFIG>
	bool checkConfiguration(int flag);


private:
	
protected:
	int SERVER = 0;
	int CLIENT = 1;
	int CONNECTED_CLIENT = 2;
	
	SOCKET_FORM socketForm;
	MESSAGE_DISPATCHER messageDispatcher;

	void activateSpecificTypeOfSocket(int type);
	
	
};

template <typename SOCKET_FORM, typename MESSAGE_DISPATCHER>
SocketType<SOCKET_FORM, MESSAGE_DISPATCHER>::~SocketType()
{
	
}


template <typename TYPE_OF_SOCKET_FORM, typename MESSAGE_DISPATCHER>
template <typename CONFIG>
void SocketType<TYPE_OF_SOCKET_FORM, MESSAGE_DISPATCHER>::setConfiguration(int config)
{
	socketForm.config.setNewFlagMemory<CONFIG>(config);
}


template <typename SOCKET_FORM, typename MESSAGE_DISPATCHER>
template <typename CONFIG>
bool SocketType<SOCKET_FORM, MESSAGE_DISPATCHER>::checkConfiguration(int flag)
{
	return ((CONFIG)socketForm.config).checkIfFlagIsActive(flag);
}

template <typename SOCKET_FORM, typename MESSAGE_DISPATCHER>
void SocketType<SOCKET_FORM, MESSAGE_DISPATCHER>::activateSpecificTypeOfSocket(int type)
{
	auto dispatchFunction = [=](char* bytes,int bytesRead)
	{
		messageDispatcher.handleBytes(bytes,bytesRead);
	};

	switch (type)
	{
	case 0:
		socketForm.activateSocketAsServer(dispatchFunction);
		break;
	}
}




