#pragma once
#include "socket_type.h"
#include "light_message_dispatcher.h"
#include "utf8_message_protocol.h"




template<typename SOCKET_FORM>
class Server : public SocketType<SOCKET_FORM,LightMessageDispatcher<UTF8MessageProtocol>>
{
typedef SocketType<SOCKET_FORM, LightMessageDispatcher<UTF8MessageProtocol>> parent;
	
friend class ListenerThread;
	
public:
	class ListenerThread : public ThreadObject
	{
	public:
		ListenerThread(Server* server):serverPointer(server){}
		~ListenerThread();

		void run() override;
	protected:
	private:
		Server* serverPointer;
	};
	
	Server();
	~Server();
	
	void activate();
	std::shared_ptr<ListenerThread> getListnerThread();
	
protected:
	
private:
	std::shared_ptr<ListenerThread> listenerThread;
	
	void listenOnNewThread();
	void listenOnMainThread();

	bool serverRunning = false;
};


template <typename SOCKET_PROTOCOL>
Server<SOCKET_PROTOCOL>::Server()
{
	
}

template <typename SOCKET_PROTOCOL>
Server<SOCKET_PROTOCOL>::~Server()
{
	
}

template <typename SOCKET_FORM>
Server<SOCKET_FORM>::ListenerThread::~ListenerThread()
{

}

template <typename SOCKET_PROTOCOL>
void Server<SOCKET_PROTOCOL>::activate()
{
	if(parent::template checkConfiguration<SocketActivationConfiguration>(RUN_ON_SECOND_THREAD))
	{
		listenerThread = ThreadManager::getThreadManager()->runFunctionObjectOnSecondThread<ListenerThread>(0,this);
	}else
	{
		
	}
}


template <typename SOCKET_FORM>
void Server<SOCKET_FORM>::ListenerThread::run()
{
	serverPointer->activateSpecificTypeOfSocket(serverPointer->SERVER);

	stopThread();

	/*serverPointer->serverRunning = true;
	auto t1 = std::chrono::high_resolution_clock::now();

	int count = 1;
	while(serverPointer->serverRunning)
	{
		serverPointer->listen();
		serverPointer->handleBytes();
		if(count == 1)
		{
			t1 = std::chrono::high_resolution_clock::now();
		}
		else if(count == 20)
		{
			break;
		}
		++count;
	}
	auto t2 = std::chrono::high_resolution_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	std::cout << "duration  " << duration << std::endl;

	stopThread();*/
}



template <typename SOCKET_FORM>
std::shared_ptr<typename Server<SOCKET_FORM>::ListenerThread> Server<SOCKET_FORM>::getListnerThread()
{
	return listenerThread;
}












