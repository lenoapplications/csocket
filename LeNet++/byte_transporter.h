#pragma once
#include <string>
class ByteTransporter
{
public:
	static const int BUFFER_SIZE = 1024;

	ByteTransporter();
	~ByteTransporter();

	void resetMemory();
	char* getBuffer();

	void setBytesRead(int bytesRead);
	int getBytesRead();

private:
	char* buffer;

	int lastNumberOfBytesRead;
protected:
};

