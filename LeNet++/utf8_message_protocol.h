#pragma once
#include "message_protocol_form.h"
#include "queue";

class UTF8MessageProtocol : public MessageProtocolForm<std::string>
{
public:
	UTF8MessageProtocol();
	~UTF8MessageProtocol();


	bool checkIfBufferIsComplete() override;
	bool getMessage(std::basic_string<char>* stringEncoding) override;
protected:
private:
};

