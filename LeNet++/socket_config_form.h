#pragma once



class SocketConfigForm
{
public:
	SocketConfigForm()=default;
	~SocketConfigForm(){};

	virtual bool checkIfFlagIsActive(int flag) const = 0;;
private:
	
protected:
	virtual void setFlag(int flags) = 0;
};