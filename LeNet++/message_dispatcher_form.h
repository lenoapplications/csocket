#pragma once
#include <string>

class ByteTransporter;


template<typename MESSAGE_PROTOCOL>
class MessageDispatcherForm
{
public:
	MessageDispatcherForm()=default;
	virtual ~MessageDispatcherForm();

	void handleBytes(char* bytes,int bytesRead);
	virtual void dispatchRawBytes(ByteTransporter* byteTransporter) = 0;
	virtual void dispatchMessage(std::string message) = 0;
	virtual void dispatchMessage(std::wstring message) = 0;
private:
protected:
	MESSAGE_PROTOCOL messageProtocol;

};


template <typename MESSAGE_PROTOCOL>
MessageDispatcherForm<MESSAGE_PROTOCOL>::~MessageDispatcherForm()
{
	
}

template <typename MESSAGE_PROTOCOL>
void MessageDispatcherForm<MESSAGE_PROTOCOL>::handleBytes(char* bytes,int bytesRead)
{
	messageProtocol.appendByteToStringBuffer(bytes,bytesRead);

	if(messageProtocol.checkIfBufferIsComplete())
	{
		std::string message;
		while(messageProtocol.getMessage(&message))
		{
			message.clear();
		}
	}
}


