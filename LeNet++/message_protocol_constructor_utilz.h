#pragma once
#include <string>
#include <sstream>



namespace basicMessageProtocol
{
	extern const char* BEGIN;
	extern const char* END;

	extern int beginCharSize;
	extern int endCharSize;


	void constructBasicMessageProtocol(std::string payload, std::string* packetBuffer);

	bool checkIfBasicMessageIsComplete(std::string* packetBuffer);

	bool checkForFirstCompletedMessage(int* begin, int* end, std::string* packetBuffer);

}




