#pragma once
#include "threadObject.h"
#include "socket_form.h"
#include "udp_meta_transfer_protocol.h"



class UdpSocket : public SocketForm
{

public:
	UdpSocket();
	~UdpSocket();

	void activateSocketAsServer(std::function<void(char*,int bytesRead)> dispatchFunction) override;
	void activateSocketAsClient() override;
	void activateSocketAsConnectedClient() override;

protected:

private:
	sockaddr_in clientsAddress;
	const int META_BUFFER_SIZE = 100;
	int MESSAGE_BUFFER_SIZE = 0;
	int sizeOfClientAddress;
	int lastMessageBufferSize = 0;
	
	char* messageBuffer;
	char* metaBuffer;
	
	void mainServerLoop(std::function<void(char*,int bytesRead)>& dispatchFunction);
	void recvMetaForIncomingMessage(int* size);
	void recvMessage(int size);
};

