#pragma once
#include <Windows.h>
#include "stringUtilz.h"
#include <string>

namespace processUtilz
{
	extern int PROCESS_SUCCESS;
	extern int FAILED_TO_START_PROCESS;

	int startNewProcess(std::wstring* commandLineArgs, STARTUPINFO* startupInfo, PROCESS_INFORMATION* processInformation);

}