#pragma once
#include <boost/interprocess/windows_shared_memory.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <string>

using namespace boost::interprocess;


namespace sharingMemoryUtilz
{
	
	typedef void* MAPPED_REGION;
	
	extern int SHARING_MEMORY_PROCESS_SUCCESS;
		
	int createNewSharedMemory(std::string shareMemoryIdent, void* object, size_t objectSize);

	int updateSharedMemory(std::string sharedMemoryIdent, void* object, size_t objectSize);

	int getSharedMemory(std::string sharedMemoryIdent, MAPPED_REGION mappedRegion,size_t size);

	int removeSharedMemory(std::string sharedMemoryIdent);
	
}

